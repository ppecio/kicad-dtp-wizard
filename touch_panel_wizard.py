from pcbnew import *
import FootprintWizardBase
import pcbnew


class PanelBuilder(object):

    def __init__(self, module, rows, cols, pitch, gap, neck, clearance, rounding):
        self.module = module
        self.rows = rows
        self.cols = cols
        self.pitch = pitch
        self.gap = gap
        self.neck = neck
        self.clearance = clearance
        self.rounding = rounding

    @property
    def pad_size(self):
        return self.pitch - self.gap

    @property
    def width(self):
        return self.cols * self.pitch

    @property
    def height(self):
        return self.rows * self.pitch

    def _get_rect_corners(self, axis):

        rect = [
            wxPoint(-self.pad_size/2, 0),
            wxPoint(0, self.pad_size/2),
            wxPoint(self.pad_size/2, 0),
            wxPoint(0, -self.pad_size/2)
        ]

        rounded_delta_h = self.rounding
        rounded_delta_v = self.rounding

        if axis == 'x' and self.gap + 2*self.rounding < self.neck + 2*self.clearance:
            rounded_delta_h = (self.neck + 2*self.clearance - self.gap)/2
        elif axis == 'y' and self.gap + 2*self.rounding < self.neck + 2*self.clearance:
            rounded_delta_v = (self.neck + 2*self.clearance - self.gap)/2

        return [
            # left
            rect[0] + wxPoint(rounded_delta_h, -rounded_delta_h),
            rect[0] + wxPoint(rounded_delta_h, rounded_delta_h),

            # bottom
            rect[1] + wxPoint(-rounded_delta_v, -rounded_delta_v),
            rect[1] + wxPoint(rounded_delta_v, -rounded_delta_v),

            # right
            rect[2] + wxPoint(-rounded_delta_h, rounded_delta_h),
            rect[2] + wxPoint(-rounded_delta_h, -rounded_delta_h),

            # top
            rect[3] + wxPoint(rounded_delta_v, rounded_delta_v),
            rect[3] + wxPoint(-rounded_delta_v, rounded_delta_v)
        ]

    # build a rectangular pad
    def smd_rect_pad(self, pos, name, axis=None, bottom=False):

        pad = D_PAD(self.module)
        pad.SetSize(wxSize(self.pad_size / 2, 0))
        pad.SetShape(PAD_SHAPE_CUSTOM)
        pad.SetAnchorPadShape(PAD_SHAPE_CIRCLE)
        pad.SetAttribute(PAD_ATTRIB_SMD)
        pad.SetLayerSet(LSET(B_Cu if bottom else F_Cu))
        pad.SetPos0(pos)
        pad.SetPosition(pos)
        pad.SetName(name)

        corners = self._get_rect_corners(axis)

        polygon = wxPoint_Vector()
        polygon.reserve(len(corners))
        for corner in corners:
            polygon.push_back(corner)

        pad.AddPrimitive(polygon, 0)

        return pad

    def smd_triangle_pad(self, pos, name, direction, bottom=False):

        pad = D_PAD(self.module)
        pad.SetSize(wxSize(self.neck, 0))
        pad.SetShape(PAD_SHAPE_CUSTOM)
        pad.SetAnchorPadShape(PAD_SHAPE_CIRCLE)
        pad.SetAttribute(PAD_ATTRIB_SMD)
        pad.SetLayerSet(LSET(B_Cu if bottom else F_Cu))
        pad.SetPos0(pos)
        pad.SetPosition(pos)
        pad.SetName(name)

        # if up/down triangles are on top layer, it's one layer design, setup axis to make space for necks
        corners = self._get_rect_corners(axis='y' if bottom is False and direction in ('u', 'd') else None)

        polygon = wxPoint_Vector()
        if direction in ('u', 'l', 'd'):
            polygon.push_back(corners[0])
            polygon.push_back(corners[1])
        if direction in ('l', 'd', 'r'):
            polygon.push_back(corners[2])
            polygon.push_back(corners[3])
        if direction in ('d', 'r', 'u'):
            polygon.push_back(corners[4])
            polygon.push_back(corners[5])
        if direction in ('r', 'u', 'l'):
            polygon.push_back(corners[6])
            polygon.push_back(corners[7])

        pad.AddPrimitive(polygon, 0)

        return pad

    def add_row_start(self, position, name, bottom):

        tp = self.smd_triangle_pad(position, name, 'r', bottom)
        self.module.Add(tp)

    def add_col_start(self, position, name, bottom):

        tp = self.smd_triangle_pad(position, name, 'd', bottom)
        self.module.Add(tp)

    def add_pad(self, position, name, axis, bottom):

        pad = self.smd_rect_pad(position, name, axis, bottom)
        self.module.Add(pad)

    def add_row_end(self, position, name, bottom):

        tp = self.smd_triangle_pad(position, name, 'l', bottom)
        self.module.Add(tp)

    def add_col_end(self, position, name, bottom):

        tp = self.smd_triangle_pad(position, name, 'u', bottom)
        self.module.Add(tp)

    def add_row(self, row_id, pos, doublesided):

        name = 'R{}'.format(row_id)

        self.add_row_start(pos, name, False)
        pos += wxPoint(self.pitch, 0)

        for n in range(1, self.cols):
            self.add_pad(pos, name, None, False)
            pos = pos + wxPoint(self.pitch, 0)

        self.add_row_end(pos, name, False)

    def add_col(self, col_id, pos, doublesided):

        name = 'C{}'.format(col_id)

        self.add_col_start(pos, name, doublesided)
        pos += wxPoint(0, self.pitch)

        for n in range(1, self.rows):
            self.add_pad(pos, name, None if doublesided else 'y', doublesided)
            pos = pos + wxPoint(0, self.pitch)

        self.add_col_end(pos, name, doublesided)

    def build_on_one_layer(self):

        x_pos = -self.width / 2
        y_pos = -self.height / 2

        pos = wxPointMM(pcbnew.ToMM(x_pos), pcbnew.ToMM(y_pos + self.pitch/2))

        # draw rows
        for r in range(self.rows):
            self.add_row(r, pos, False)
            pos += wxPoint(0, self.pitch)

        pos = wxPointMM(pcbnew.ToMM(x_pos + self.pitch/2), pcbnew.ToMM(y_pos))

        # draw cols
        for c in range(self.cols):
            self.add_col(c, pos, False)
            pos += wxPoint(self.pitch, 0)

    def build_on_two_layers(self):

        x_pos = -self.width / 2
        y_pos = -self.height / 2

        pos = wxPointMM(pcbnew.ToMM(x_pos), pcbnew.ToMM(y_pos + self.pitch/2))

        # draw rows
        for r in range(self.rows):
            self.add_row(r, pos, True)
            pos += wxPoint(0, self.pitch)

        pos = wxPointMM(pcbnew.ToMM(x_pos + self.pitch/2), pcbnew.ToMM(y_pos))

        # draw cols
        for c in range(self.cols):
            self.add_col(c, pos, True)
            pos += wxPoint(self.pitch, 0)


class TouchPanelWizard(FootprintWizardBase.FootprintWizard):

    def GetName(self):
        """
        Return footprint name.
        This is specific to each footprint class, you need to implement this
        """
        return 'Touch Panel'

    def GetDescription(self):
        """
        Return footprint description.
        This is specific to each footprint class, you need to implement this
        """
        return 'Capacitive diamond pattern touch panel'

    def GetValue(self):
        return "TouchPanel-{p}-{c}x{r}".format(
            c=self.basic['cols'],
            r=self.basic['rows'],
            p=pcbnew.ToMM(self.pads['pitch'])
        )

    def GenerateParameterList(self):
        board = pcbnew.GetBoard()
        ds = board.GetDesignSettings()

        self.AddParam("Basic", "rows", self.uInteger, 6, min_value=3)
        self.AddParam("Basic", "cols", self.uInteger, 9, min_value=3)
        self.AddParam("Basic", "one_layer", self.uBool, False)

        self.AddParam("Pads", "pitch", self.uMM, 6, min_value=0)
        self.AddParam("Pads", "gap", self.uMM, pcbnew.ToMM(ds.GetSmallestClearanceValue())*2, min_value=0)
        self.AddParam("Pads", "neck", self.uMM, pcbnew.ToMM(ds.GetCurrentTrackWidth()), min_value=0)
        self.AddParam("Pads", "clearance", self.uMM, pcbnew.ToMM(ds.GetSmallestClearanceValue()), min_value=0)
        self.AddParam("Pads", "rounding", self.uMM, pcbnew.ToMM(ds.GetCurrentTrackWidth())/2, min_value=0)

        self.AddParam("Elements", "outline", self.uBool, True)

    @property
    def pads(self):
        return self.parameters['Pads']

    @property
    def basic(self):
        return self.parameters['Basic']

    @property
    def elements(self):
        return self.parameters['Elements']

    # This method checks the parameters provided to wizard and set errors
    def CheckParameters(self):
        # TODO - implement custom checks
        pass

    def BuildThisFootprint(self):

        cols = self.basic["cols"]
        rows = self.basic["rows"]
        pitch = self.pads["pitch"]
        gap = self.pads["gap"]
        neck = self.pads['neck']
        clear = self.pads['clearance']
        round = self.pads['rounding']

        # set SMD attribute
        self.module.SetAttributes(MOD_CMS)

        builder = PanelBuilder(self.module, rows, cols, pitch, gap, neck, clear, round)
        if self.basic['one_layer']:
            builder.build_on_one_layer()
        else:
            builder.build_on_two_layers()

        x_pos = builder.width / 2 + gap
        y_pos = builder.height / 2 + gap

        t_size = self.GetTextSize()
        linewidth = self.draw.GetLineThickness()

        y_text = y_pos + t_size + gap

        self.draw.Value(x_pos/2, y_text, t_size)
        self.draw.Reference(-x_pos/2, y_text, t_size)

        if not self.elements['outline']:
            return

        # outline
        self.draw.Box(0, 0, builder.width + 2*gap, builder.height + 2*gap)

        length = t_size

        line_y = y_pos - gap

        # draw rows marker
        for r in range(rows+1):
            self.draw.Line(-x_pos-2*gap-length, line_y, -x_pos-2*gap, line_y)
            line_y -= pitch

        line_x = x_pos - gap

        # draw cols marker
        for r in range(cols+1):
            self.draw.Line(line_x, -y_pos-2*gap-length, line_x, -y_pos-2*gap)
            line_x -= pitch


TouchPanelWizard().register()

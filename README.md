# Kicad Diamond Touch Panel Wizard

Layout generator is based on information from MTCH6102 controller application 
note and article https://www.thinkmind.org/download.php?articleid=sensordevices_

## Usage
Download plugin file and place in KiCad plugins directory 
(eg on Linux ~/.kicad/scripting/plugins)

Open KiCad Footprint Editor and create new one using wizard dialog. 
Choose Diamond Touch Panel wizard from the list of available wizards.

Available options:
* cols - number of columns in the panel
* rows - number of rows in the panel
* one_layer - if checked panel is generated on F_Cu layer only, rows are 
indeed to be connected on F_Cu layer. Algorithm ensures that there is enough 
space between diamonds in columns to draw row necks.
* pitch - pitch size (segment size)
* gap - distance between pads
* neck - size of neck tracks which connects pads
* clearance - distance between neck and pad
* rounding - pads rounding, currently it just cuts pad corners
* outline - determine if outline on drawing layer should be rendered

Currently wizard draws only pads, which should be later connected manually
using tracks. 

Rows pads are named as R0,R1,R2..., columns pads as C0,C1,C2...

MR which adds neck drawing in one and two layers layout is welcomed.

